function string4(obj)
{
    if(typeof obj != 'object')
        return "";
        
    const arr = Object.values(obj).map(ele => {
        return ele.charAt(0).toUpperCase() +
        ele.slice(1).toLowerCase();
      });
    
    return arr.join(' ');
}

module.exports = string4;
