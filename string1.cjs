function string1(str)
{
    if(typeof(str) !== 'string')
        return Number(0);
    
    str = str.split(',').join('');
    str = str.split('$').join('');
    if(isNaN(str))
        return Number(0);
    else
        return Number(str);
    
}

module.exports = string1;
