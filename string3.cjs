function string3(str)
{
    if(typeof(str) !== 'string')
        return "";
    
    let ans = str.split('/');

    if(ans[0]<0 || ans[0]>31 || ans[1]<0 || ans[1]>12 || ans[2]<0 || ans.length !== 3)
        return "";

    switch(Number(ans[1]))
    {
        case 1:
            return "January";
        case 2:
            return "February";
        case 3:
            return "March";
        case 4:
            return "April";
        case 5:
            return "May";
        case 6:
            return "June";
        case 7:
            return "July";
        case 8:
            return "August";
        case 9:
            return "September";
        case 10:
            return "October";
        case 11:
            return "November";
        case 12:
            return "December";
    }
    return "";
}

module.exports = string3;
