function string2(str)
{
    if(typeof(str) !== 'string')
        return [];
    let ans = str.split('.');
    ans = ans.map(Number);

    if(ans.length === 4 && onlyNumbers(ans)===true)
        return ans;
    else
        return [];
}

function onlyNumbers(array) {
    return array.every(element => {
      return !isNaN(element);
    });
  }

module.exports = string2;
