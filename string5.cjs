function string5(arr)
{
    if(Array.isArray(arr) === false || arr === undefined || arr.length==0 )
        return "";
    
    return arr.join(' ');
}

module.exports = string5;
